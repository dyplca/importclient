(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.DyplcaImportService) {
      root.DyplcaImportService = {};
    }
    root.DyplcaImportService.InlineResponse200 = factory(root.DyplcaImportService.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The InlineResponse200 model module.
   * @module model/InlineResponse200
   * @version 0.0.0
   */

  /**
   * Constructs a new <code>InlineResponse200</code>.
   * @alias module:model/InlineResponse200
   * @class
   */
  var exports = function() {





  };

  /**
   * Constructs a <code>InlineResponse200</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/InlineResponse200} obj Optional instance to populate.
   * @return {module:model/InlineResponse200} The populated <code>InlineResponse200</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('supplyCount')) {
        obj['supplyCount'] = ApiClient.convertToType(data['supplyCount'], 'Integer');
      }
      if (data.hasOwnProperty('processCount')) {
        obj['processCount'] = ApiClient.convertToType(data['processCount'], 'Integer');
      }
      if (data.hasOwnProperty('interventionCount')) {
        obj['interventionCount'] = ApiClient.convertToType(data['interventionCount'], 'Integer');
      }
      if (data.hasOwnProperty('emissionCount')) {
        obj['emissionCount'] = ApiClient.convertToType(data['emissionCount'], 'Integer');
      }
    }
    return obj;
  }


  /**
   * @member {Integer} supplyCount
   */
  exports.prototype['supplyCount'] = undefined;

  /**
   * @member {Integer} processCount
   */
  exports.prototype['processCount'] = undefined;

  /**
   * @member {Integer} interventionCount
   */
  exports.prototype['interventionCount'] = undefined;

  /**
   * @member {Integer} emissionCount
   */
  exports.prototype['emissionCount'] = undefined;




  return exports;
}));
