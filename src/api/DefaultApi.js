(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', '../model/InlineResponse200', '../model/Error'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/InlineResponse200'), require('../model/Error'));
  } else {
    // Browser globals (root is window)
    if (!root.DyplcaImportService) {
      root.DyplcaImportService = {};
    }
    root.DyplcaImportService.DefaultApi = factory(root.DyplcaImportService.ApiClient, root.DyplcaImportService.InlineResponse200, root.DyplcaImportService.Error);
  }
}(this, function(ApiClient, InlineResponse200, Error) {
  'use strict';

  /**
   * Default service.
   * @module api/DefaultApi
   * @version 0.0.0
   */

  /**
   * Constructs a new DefaultApi. 
   * @alias module:api/DefaultApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use, default to {@link module:ApiClient#instance}
   * if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the importPost operation.
     * @callback module:api/DefaultApi~importPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/InlineResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Import project file\n
     * @param {String} file file path
     * @param {String} projectId project ObjectId
     * @param {module:api/DefaultApi~importPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/InlineResponse200}
     */
    this.importPost = function(file, projectId, callback) {
      var postBody = null;

      // verify the required parameter 'file' is set
      if (file == undefined || file == null) {
        throw "Missing the required parameter 'file' when calling importPost";
      }

      // verify the required parameter 'projectId' is set
      if (projectId == undefined || projectId == null) {
        throw "Missing the required parameter 'projectId' when calling importPost";
      }


      var pathParams = {
      };
      var queryParams = {
        'file': file,
        'projectId': projectId
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = [];
      var returnType = InlineResponse200;

      return this.apiClient.callApi(
        '/import', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
