# DyplcaImportService.InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplyCount** | **Integer** |  | [optional] 
**processCount** | **Integer** |  | [optional] 
**interventionCount** | **Integer** |  | [optional] 
**emissionCount** | **Integer** |  | [optional] 


