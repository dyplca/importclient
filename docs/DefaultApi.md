# DyplcaImportService.DefaultApi

All URIs are relative to *http://import-service:9080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**importPost**](DefaultApi.md#importPost) | **POST** /import | 


<a name="importPost"></a>
# **importPost**
> InlineResponse200 importPost(file, projectId)



Import project file\n

### Example
```javascript
var DyplcaImportService = require('dyplca-import-service');

var apiInstance = new DyplcaImportService.DefaultApi()

var file = "file_example"; // {String} file path

var projectId = "projectId_example"; // {String} project ObjectId


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.importPost(file, projectId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **String**| file path | 
 **projectId** | **String**| project ObjectId | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

